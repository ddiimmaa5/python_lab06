def multiple_replace(target_str, replace_values):
    for i, j in replace_values.items():
        target_str = target_str.replace(i, j)
    return target_str


repl = {'"0,00"': '0.00', '"0,50"': '0.50', '"1,00"': '1.00', '"1,50"': '1.50', '"2,00"': '2.00', '"2,50"': '2.50', '"3,00"': '3.00', '"3,50"': '3.50', '"4,00"': '4.00', '"4,50"': '4.50', '"5,00"': '5.00', '"5,50"': '5.50', '"6,00"': '6.00', '"6,50"': '6.50', '"7,00"': '7.00', '"7,50"': '7.50', '"8,00"': '8.00', '"8,50"': '8.50', '"9,00"': '9.00', '"9,50"': '9.50', '"10,00"': '10.00'}
f = open('marks.lab6.csv', 'r')
str_count = 0
counts = []
start_time = []
end_time = []
time = []
best_counts = []
count1 = []
count2 = []
count3 = []
count4 = []
count5 = []
count6 = []
count7 = []
count8 = []
count9 = []
count10 = []
count11 = []
count12 = []
count13 = []
count14 = []
count15 = []
count16 = []
count17 = []
count18 = []
count19 = []
count20 = []
for line in f:
    str_count += 1
    data = multiple_replace(line, repl).replace(',', ' ').split()
    counts.append(data[15])
    start_time.append(data[4].replace(':', '.'))
    end_time.append(data[9].replace(':', '.'))
    count1.append(data[-1])
    count2.append(data[-2])
    count3.append(data[-3])
    count4.append(data[-4])
    count5.append(data[-5])
    count6.append(data[-6])
    count7.append(data[-7])
    count8.append(data[-8])
    count9.append(data[-9])
    count10.append(data[-10])
    count11.append(data[-11])
    count12.append(data[-12])
    count13.append(data[-13])
    count14.append(data[-14])
    count15.append(data[-15])
    count16.append(data[-16])
    count17.append(data[-17])
    count18.append(data[-18])
    count19.append(data[-19])
    count20.append(data[-20])
print(f'Кількість студентів які проходили тестування {str_count}')
f.close()
f = open('Task7.txt', 'w')
i = 0.00
while i <= 10.00:
    a = str(i)
    a += '0'
    i += 0.50
    print(f'Студенти які отримали {a} балів: {counts.count(a)}')
for i in range(len(start_time)):
    time.append((float(end_time[i]) - float(start_time[i]))*100)
    best_counts.append(round(float(counts[i]) / time[i], 2))
    print(f'Студент {i}, середній бал в хвилину {round(float(counts[i])/ time[i], 2)}')
best_counts.sort(reverse=True)
true = '0.50'
f.write(f'Завдання 1, відсоток правильних відповідей: {round(count1.count(true) / str_count * 100, 1)}, відсоток не правильних відповідей {round(100 - count1.count(true) / str_count * 100, 1)}\n')
f.write(f'Завдання 2, відсоток правильних відповідей: {round(count2.count(true) / str_count * 100, 1)}, відсоток не правильних відповідей {round(100 - count2.count(true) / str_count * 100, 1)}\n')
f.write(f'Завдання 3, відсоток правильних відповідей: {round(count3.count(true) / str_count * 100, 1)}, відсоток не правильних відповідей {round(100 - count3.count(true) / str_count * 100, 1)}\n')
f.write(f'Завдання 4, відсоток правильних відповідей: {round(count4.count(true) / str_count * 100, 1)}, відсоток не правильних відповідей {round(100 - count4.count(true) / str_count * 100, 1)}\n')
f.write(f'Завдання 5, відсоток правильних відповідей: {round(count5.count(true) / str_count * 100, 1)}, відсоток не правильних відповідей {round(100 - count5.count(true) / str_count * 100, 1)}\n')
f.write(f'Завдання 6, відсоток правильних відповідей: {round(count6.count(true) / str_count * 100, 1)}, відсоток не правильних відповідей {round(100 - count6.count(true) / str_count * 100, 1)}\n')
f.write(f'Завдання 7, відсоток правильних відповідей: {round(count7.count(true) / str_count * 100, 1)}, відсоток не правильних відповідей {round(100 - count7.count(true) / str_count * 100, 1)}\n')
f.write(f'Завдання 8, відсоток правильних відповідей: {round(count8.count(true) / str_count * 100, 1)}, відсоток не правильних відповідей {round(100 - count8.count(true) / str_count * 100, 1)}\n')
f.write(f'Завдання 9, відсоток правильних відповідей: {round(count9.count(true) / str_count * 100, 1)}, відсоток не правильних відповідей {round(100 - count9.count(true) / str_count * 100, 1)}\n')
f.write(f'Завдання 10, відсоток правильних відповідей: {round(count10.count(true) / str_count * 100, 1)}, відсоток не правильних відповідей {round(100 - count10.count(true) / str_count * 100, 1)}\n')
f.write(f'Завдання 11, відсоток правильних відповідей: {round(count11.count(true) / str_count * 100, 1)}, відсоток не правильних відповідей {round(100 - count11.count(true) / str_count * 100, 1)}\n')
f.write(f'Завдання 12, відсоток правильних відповідей: {round(count12.count(true) / str_count * 100, 1)}, відсоток не правильних відповідей {round(100 - count12.count(true) / str_count * 100, 1)}\n')
f.write(f'Завдання 13, відсоток правильних відповідей: {round(count13.count(true) / str_count * 100, 1)}, відсоток не правильних відповідей {round(100 - count13.count(true) / str_count * 100, 1)}\n')
f.write(f'Завдання 14, відсоток правильних відповідей: {round(count14.count(true) / str_count * 100, 1)}, відсоток не правильних відповідей {round(100 - count14.count(true) / str_count * 100, 1)}\n')
f.write(f'Завдання 15, відсоток правильних відповідей: {round(count15.count(true) / str_count * 100, 1)}, відсоток не правильних відповідей {round(100 - count15.count(true) / str_count * 100, 1)}\n')
f.write(f'Завдання 16, відсоток правильних відповідей: {round(count16.count(true) / str_count * 100, 1)}, відсоток не правильних відповідей {round(100 - count16.count(true) / str_count * 100, 1)}\n')
f.write(f'Завдання 17, відсоток правильних відповідей: {round(count17.count(true) / str_count * 100, 1)}, відсоток не правильних відповідей {round(100 - count17.count(true) / str_count * 100, 1)}\n')
f.write(f'Завдання 18, відсоток правильних відповідей: {round(count18.count(true) / str_count * 100, 1)}, відсоток не правильних відповідей {round(100 - count18.count(true) / str_count * 100, 1)}\n')
f.write(f'Завдання 19, відсоток правильних відповідей: {round(count19.count(true) / str_count * 100, 1)}, відсоток не правильних відповідей {round(100 - count19.count(true) / str_count * 100, 1)}\n')
f.write(f'Завдання 20, відсоток правильних відповідей: {round(count20.count(true) / str_count * 100, 1)}, відсоток не правильних відповідей {round(100 - count20.count(true) / str_count * 100, 1)}\n')
f.write('5 Найкращих оцінок:')
for i in range(6):
    f.write(f' {best_counts[i]}')
f.close()